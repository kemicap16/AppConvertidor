package facci.kellycastillo.conversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityA extends AppCompatActivity {

    EditText txtFahrenheit;
    EditText txtCentigrados;
    EditText txtMuestraR;
    Button btnConversionC;
    Button btnConversionF;
    Button btnBorrarD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        Log.i("ActivityA", "Kelly Mishell Castillo Palma");


        txtFahrenheit =(EditText) findViewById(R.id.txtFahr);
        txtCentigrados =(EditText) findViewById(R.id.txtCent);
        txtMuestraR = (EditText) findViewById(R.id.txtMostrar);
        btnConversionC = (Button) findViewById(R.id.btnConvertirC);
        btnConversionF = (Button) findViewById(R.id.btnConvertirF);
        btnBorrarD = (Button) findViewById(R.id.btnBorrar);


        btnConversionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num1 = Float.parseFloat(txtFahrenheit.getText().toString());
                txtMuestraR.setText(String.valueOf(Convertir.convertirF_C(num1)));
            }
        });

        btnConversionF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float num2 = Float.parseFloat(txtCentigrados.getText().toString());
                txtMuestraR.setText(String.valueOf(Convertir.convertirC_F(num2)));
            }
        });

        btnBorrarD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtFahrenheit.setText("");
                txtCentigrados.setText("");
                txtMuestraR.setText("");
            }
        });
    }

    public static class Convertir {

        public static float convertirF_C(float fahrenheit) {
            return ((fahrenheit - 32) * 5 / 9);
        }

        public static float convertirC_F(float centigrados) {
            return ((centigrados * 9) / 5) + 32;
        }
    }
}
